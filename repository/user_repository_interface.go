package repository

import "webExample/entity"

type Writer interface {
	Create(e *entity.User) error
}

type Reader interface {
	Get(id int64) (*entity.User, error)
}

type UserRepositoryInterface interface {
	Writer
	Reader
}
