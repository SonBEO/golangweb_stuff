package repository

import (
	"database/sql"
	"webExample/entity"
)

type UserRepository struct {
	DB *sql.DB
}

func NewUserRepository(db *sql.DB) *UserRepository {
	return &UserRepository{DB: db}
}

func (r *UserRepository) Create(e *entity.User) error {
	query, err := r.DB.Prepare(`Insert into users(first_name, last_name, email, password)
	values (?,?,?,?)`)
	if err != nil {
		return err
	}
	_, err = query.Exec(
		e.FirstName,
		e.LastName,
		e.Email,
		e.Password,
	)
	if err != nil {
		return err
	}
	err = query.Close()
	if err != nil {
		return err
	}
	return nil
}

func (r *UserRepository) Get(id int64) (*entity.User, error) {
	//to-do code block
	return nil, nil
}
