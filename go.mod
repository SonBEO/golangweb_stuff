module webExample

go 1.21.5

require github.com/go-sql-driver/mysql v1.7.1

require (
	github.com/stretchr/testify v1.8.4
	golang.org/x/crypto v0.0.0-20191011191535-87dc89f01550
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
