package usecase

type UserUseCaseInterface interface {
	CreateUser(FirstName, LastName, Email, Password string) error
}
