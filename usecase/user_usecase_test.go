package usecase_test

import (
	"database/sql"
	"testing"
	"webExample/entity"
	"webExample/repository"
	"webExample/usecase"

	_ "github.com/go-sql-driver/mysql"
	"github.com/stretchr/testify/assert"
)

func Test_Create(t *testing.T) {
	db, _ := sql.Open("mysql", "user:userPassword@tcp(localhost:3306)/sampleDB")
	userTestRepo := repository.NewUserRepository(db)
	userTestUseCase := usecase.NewService(userTestRepo)
	user := CreateTestUser()
	err2 := userTestUseCase.CreateUser(user.Email, user.Password, user.FirstName, user.LastName)
	assert.Nil(t, err2)
}

func CreateTestUser() *entity.User {
	return &entity.User{
		FirstName: "Tester",
		LastName:  "San",
		Email:     "tester@gmail.com",
		Password:  "IamATester",
	}
}
