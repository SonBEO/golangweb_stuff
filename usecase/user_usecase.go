package usecase

import (
	"webExample/entity"
	"webExample/repository"
)

type Service struct {
	// user repository instance
	userUsecase repository.UserRepositoryInterface
}

func NewService(r repository.UserRepositoryInterface) *Service {
	return &Service{userUsecase: r}
}

func (u *Service) CreateUser(email, password, firstName, lastName string) error {
	user, err := entity.NewUser(email, password, firstName, lastName)
	if err != nil {
		return err
	}
	return u.userUsecase.Create(user)
}
