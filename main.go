package main

import (
	"database/sql"
	"fmt"
	"time"
	"webExample/repository"
	"webExample/usecase"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	db, err := sql.Open("mysql", "user:userPassword@tcp(localhost:3306)/sampleDB")

	if err != nil {
		panic(fmt.Errorf("connection fail: %v", err))
	}

	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS users(
			id int(10) unsigned NOT NULL AUTO_INCREMENT,
			first_name varchar(255) NOT NULL,
			last_name varchar(255) NOT NULL,
			email varchar(255) NOT NULL,
			password varchar(255) NOT NULL,
			created_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
			updated_at timestamp NULL DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY(id)
		);`)
	if err != nil {
		panic(fmt.Errorf("executions fail: %v", err))
	}

	fmt.Println("Connection success")

	// create a user

	userRepo := repository.NewUserRepository(db)
	userUseCase := usecase.NewService(userRepo)
	userUseCase.CreateUser("caotrung@gmail.com", "Reiskoishi", "Cao", "Trung")
	fmt.Println("User Added")
}
